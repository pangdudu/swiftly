# Loads mkmf which is used to make makefiles for Ruby extensions
require 'mkmf'

#we need some extra libs from swift, needs to be installed, fix the path if it's somewhere else
$LOCAL_LIBS += " /opt/swift/lib/libswift.so"

# Give it a name
extension_name = 'swiftly'

# The destination
dir_config(extension_name)

# Do the work
create_makefile(extension_name)
