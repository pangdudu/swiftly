// Include the Ruby headers and goodies
#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../external/include/swift.h"

// Defining a space for information and references about the module to be stored internally
VALUE Swiftly = Qnil;

// Prototype for the initialization method - Ruby calls this, not you
void Init_swiftly();

// Prototype for our method 'speak' - methods are prefixed by 'method_' here
VALUE method_speak(VALUE self, VALUE text);

// The initialization method for this module
void Init_swiftly() {
	Swiftly = rb_define_module("Swiftly");
	rb_define_method(Swiftly, "speak", method_speak, 1);
}

// Our 'speak' method
VALUE method_speak(VALUE self, VALUE text) {
    char* ctext = STR2CSTR(text);
    swift_engine *engine;
    swift_port *port = NULL;

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL) {
        fprintf(stderr, "Failed to open Swift Engine.");
        goto all_done;
    }
    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, NULL)) == NULL) {
        fprintf(stderr, "Failed to open Swift Port.");
        goto all_done;
    }

    /* Synthesize argument as a text string */
    swift_port_speak_text(port, ctext, 0, NULL, NULL, NULL); 
    
all_done:
    /* Close the Swift Port and Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);	return 1;
}
