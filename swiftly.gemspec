Gem::Specification.new do |s|
  s.name = %q{swiftly}
  s.version = "1.0.1"

  s.specification_version = 2 if s.respond_to? :specification_version=

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-09-17}
  s.default_executable = %q{swiftly}
  s.description = %q{swiftly!}
  s.email = %q{pangdudu@github}
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["lib/Swiftly.c",
  "lib/extconf.rb",
  "test/test_swiftly.rb",
  "external/include",
  "external/include/swift_exports.h",
  "external/include/swift_defs.h",
  "external/include/swift.h",
  "external/include/swift_params.h", "README.rdoc"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/swiftly}
  s.rubyforge_project = %q{http://github.com/pangdudu/swiftly}
  s.require_paths = ["lib","external/include"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{more swiftly!}
end
